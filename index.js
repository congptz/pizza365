const express = require("express"); // Tương tự : import express from "express";
const path = require("path");

// Khởi tạo Express App
const app = express();

// Khai báo ứng dụng sẽ chạy trên cổng 8000
const port = 8000;

//Khai báo mongoose
var mongoose = require('mongoose');

//khai báo routers
const drinkRouter = require("./app/routers/drink.router");
const voucherRouter = require("./app/routers/voucher.router");
const userRouter = require("./app/routers/user.router");
const orderRouter = require("./app/routers/order.router");
const userFilterRouter = require("./app/routers/userFilter.router");
const NR4Router = require("./app/routers/NR4.router");

// Khai báo ứng dụng đọc được body raw json (Build in middleware)
app.use(express.json());

//kết nối mongodb
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
  .then(() => console.log("Connected to Mongo Successfully"))
  .catch(error => handleError(error));
//khai báo các model
const drinkModel = require("./app/models/drink.model.js");
const voucherModel = require("./app/models/voucher.model.js");
const orderModel = require("./app/models/order.model.js");
const userModel = require("./app/models/user.model.js");

//load các thành phần tĩnh như ảnh, css
app.use(express.static(__dirname + "/views"))

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/index.html"))
})

app.use("/api/v1/drinks", drinkRouter);
app.use("/api/v1/vouchers", voucherRouter);
app.use("/api/v1/users", userRouter);
app.use("/api/v1/orders", orderRouter);
app.use("/api/v1/filter-user", userFilterRouter);
app.use("/devcamp-pizza365", NR4Router);

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})

module.exports = app;