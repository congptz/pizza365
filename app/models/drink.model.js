//b1: khai báo thư viện mongo
const mongoose = require("mongoose")

//b2: khai báo class Schema
const Schema = mongoose.Schema

//b3: khởi tạo Shema với các thuộc tính của collection
const drinkSchema = new Schema ({
    _id: mongoose.Types.ObjectId, //có thể khai báo hoặc không, vì mongoose thường tự sinh
    maNuocUong: {
        type: String,
        unique: true,
        required: true,
    },
    tenNuocUong: {
        type: String,
        required: true,
    },
    donGia: {
        type: Number,
        required: true,
    }
});

//b4:biên dịch shema thành model
module.exports = mongoose.model("drink", drinkSchema);