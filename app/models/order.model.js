//b1: khai báo thư viện mongo
const mongoose = require("mongoose")

//b2: khai báo class Schema
const Schema = mongoose.Schema

//b3: khởi tạo Shema với các thuộc tính của collection
const orderSchema = new Schema ({
    _id: mongoose.Types.ObjectId, //có thể khai báo hoặc không, vì mongoose thường tự sinh
    orderCode: {
        type: String,
        unique: true,
    },
    pizzaSize: {
        type: String,
        required: true,
    },
    pizzaType: {
        type: String,
        required: true,
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "voucher"
    },
    drink: {
        type: mongoose.Types.ObjectId,
        ref: "drink"
    },
    status: {
        type: String,
        required: true,
    }
});

//b4:biên dịch shema thành model
module.exports = mongoose.model("order", orderSchema);