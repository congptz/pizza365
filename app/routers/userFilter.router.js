const express = require("express");

const userController = require("../controllers/user.controller");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request method: ", req.method);

    next();
});

router.get("/limit-users", userController.getAllLimitUser);

router.get("/skip-users", userController.getAllSkipUser);

router.get("/sort-users", userController.getAllSortUser);

router.get("/skip-limit-users", userController.getAllSkipLimitUser);

router.get("/sort-skip-limit-users", userController.getAllSortSkipLimitUser);

module.exports = router;