const express = require("express");

//const userMiddleware = require("../middlewares/user.middleware");
const userController = require("../controllers/user.controller");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request method: ", req.method);

    next();
});

router.get("/", /*userMiddleware.getAllUserMiddleware,*/ userController.getAllUser)

router.post("/", userController.createUser);

router.get("/:userId", userController.getUserById);

router.put("/:userId", userController.updateUser);

router.delete("/:userId", userController.deleteUser);

module.exports = router;