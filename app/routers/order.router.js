const express = require("express");

//const orderMiddleware = require("../middlewares/order.middleware");
const orderController = require("../controllers/order.controller");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request method: ", req.method);

    next();
});

router.get("/", /*orderMiddleware.getAllOrderMiddleware,*/ orderController.getAllOrder)

router.post("/", orderController.createOrder);

router.get("/:orderId", orderController.getOrderById);

router.put("/:orderId", orderController.updateOrder);

router.delete("/:orderId", orderController.deleteOrder);

module.exports = router;