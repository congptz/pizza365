const express = require("express");

//const voucherMiddleware = require("../middlewares/voucher.middleware");
const voucherController = require("../controllers/voucher.controller");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request method: ", req.method);

    next();
});

router.get("/", /*voucherMiddleware.getAllVoucherMiddleware,*/ voucherController.getAllVoucher)

router.post("/", voucherController.createVoucher);

router.get("/:voucherId", voucherController.getVoucherById);

router.put("/:voucherId", voucherController.updateVoucher);

router.delete("/:voucherId", voucherController.deleteVoucher);

module.exports = router;