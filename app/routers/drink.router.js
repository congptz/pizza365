const express = require("express");

//const drinkMiddleware = require("../middlewares/drink.middleware");
const drinkController = require("../controllers/drink.controller");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request method: ", req.method);

    next();
});

router.get("/", /*drinkMiddleware.getAllDrinkMiddleware,*/ drinkController.getAllDrink)

router.post("/", drinkController.createDrink);

router.get("/:drinkId", drinkController.getDrinkById);

router.put("/:drinkId", drinkController.updateDrink);

router.delete("/:drinkId", drinkController.deleteDrink);

module.exports = router;