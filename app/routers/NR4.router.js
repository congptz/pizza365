const express = require("express");

const NR4Controller = require("../controllers/NR4.controller");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request method: ", req.method);

    next();
});

router.post("/orders", NR4Controller.createOrder);

router.post("/drinks", NR4Controller.getAllDrink);

module.exports = router;