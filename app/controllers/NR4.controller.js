//import thư viện mongoose
const mongoose = require('mongoose');

// import order model
const orderModel = require('../models/order.model');

// import user model
const userModel = require('../models/user.model');

//import drink model
const drinkModel = require('../models/drink.model');

const createOrder = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        userId,
        orderCode,
        pizzaSize,
        pizzaType,
        voucher,
        drink,
        status,
    } = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "User ID is not valid"
        })
    }
    if (!orderCode) {
        return res.status(400).json({
            status: "Bad request",
            message: "orderCode is required!"
        })
    }
    if (!pizzaSize) {
        return res.status(400).json({
            status: "Bad request",
            message: "pizzaSize is required!"
        })
    }
    if (!pizzaType) {
        return res.status(400).json({
            status: "Bad request",
            message: "pizzaType is required!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Voucher ID is not valid"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(drink)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Drink ID is not valid"
        })
    }
    if (!status) {
        return res.status(400).json({
            status: "Bad request",
            message: "status is required!"
        })
    }
    
    const UserExist = await userModel.findById(userId).exec();
    console.log(UserExist);
    // B3: Thao tác với CSDL
    if(UserExist != null) {
        var newOrder = {
            _id: new mongoose.Types.ObjectId(),
            userId,
            orderCode,
            pizzaSize,
            pizzaType,
            voucher: new mongoose.Types.ObjectId(voucher),
            drink: new mongoose.Types.ObjectId(drink),
            status,
        }
    
        try {
            const createdOrder = await orderModel.create(newOrder);
    
            const updatedUser = await userModel.findByIdAndUpdate(userId, {
                $push: { orders: createdOrder._id }
            })
    
            return res.status(201).json({
                status: "Create order successfully",
                user: updatedUser,
                data: createdOrder
            })
        } catch (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
    } else {
        return res.status(400).json({
            status: "Bad request",
            message: "Not found any user!"
        })
    }
    
}

const getAllDrink = (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    drinkModel.find()
                .then((data) => {
                    if (data && data.length > 0) {
                        return res.status(200).json({
                            status:"Get all drinks sucessfully",
                            data
                        })
                    } else {
                        return res.status(404).json({
                            status:"Not found any drink",
                            data
                        })
                    }

                })
                .catch((error) => {
                    return res.status(500).json({
                        status:"Internal Server Error",
                        message:error.message
                    })
                })
}

module.exports = {
    createOrder,
    getAllDrink,
}