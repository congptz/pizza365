//import thư viện mongoose
const mongoose = require('mongoose');

// import user model
const userModel = require('../models/user.model');

//viết api có async await
const createUser = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        fullName,
        email,
        address,
        phone,
    } = req.body;

    // B2: Validate dữ liệu
    if (!fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is required!"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required!"
        })
    }
    if (!address) {
        return res.status(400).json({
            status: "Bad request",
            message: "address is required!"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required!"
        })
    }

    // B3: Thao tác với CSDL
    var newUser = {
        _id: new mongoose.Types.ObjectId(),
        fullName,
        email,
        address,
        phone,
    }

    try {
        const createdUser = await userModel.create(newUser);

        return res.status(201).json({
            status: "Create user successfully",
            data: createdUser
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const getAllUser = async (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    try {
        const userList = await userModel.find();

        if (userList && userList.length > 0) {
            return res.status(200).json({
                status: "Get all users sucessfully",
                data: userList
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const getUserById = async (req, res) => {
    //B1: thu thập dữ liệu
    var userId = req.params.userId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const userInfo = await userModel.findById(userId);

        if (userInfo) {
            return res.status(200).json({
                status: "Get user by id sucessfully",
                data: userInfo
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const updateUser = async (req, res) => {
    //B1: thu thập dữ liệu
    var userId = req.params.userId;

    const {
        fullName,
        email,
        address,
        phone,
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (!fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is required!"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required!"
        })
    }
    if (!address) {
        return res.status(400).json({
            status: "Bad request",
            message: "address is required!"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required!"
        })
    }

    //B3: thực thi model
    try {
        let updateUser = {
            fullName,
            email,
            address,
            phone,
        }

        const updatedUser = await userModel.findByIdAndUpdate(userId, updateUser);

        if (updatedUser) {
            return res.status(200).json({
                status: "Update user sucessfully",
                data: updatedUser
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteUser = async (req, res) => {
    //B1: Thu thập dữ liệu
    var userId = req.params.userId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    //B3: thực thi model
    try {
        const deletedUser = await userModel.findByIdAndDelete(userId);

        if (deletedUser) {
            return res.status(200).json({
                status: "Delete user sucessfully",
                data: deletedUser
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const getAllLimitUser = async (req, res) => {
    //B1: thu thập dữ liệu
    var limit = req.query.limit;
    console.log(limit);
    //B2: kiểm tra
    //B3: thực thi model
    try {
        const userList = await userModel.find().limit(limit);

        if (userList && userList.length > 0) {
            return res.status(200).json({
                status: "Get all user limit sucessfully",
                data: userList
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const getAllSkipUser = async (req, res) => {
    //B1: thu thập dữ liệu
    var skip = req.query.skip;
    console.log(skip);
    //B2: kiểm tra
    //B3: thực thi model
    try {
        const userList = await userModel.find().skip(skip);

        if (userList && userList.length > 0) {
            return res.status(200).json({
                status: "Get all user skip sucessfully",
                data: userList
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const getAllSortUser = async (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    try {
        const userList = await userModel.find().sort({ fullName: 1});

        if (userList && userList.length > 0) {
            return res.status(200).json({
                status: "Get all user sort sucessfully",
                data: userList
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const getAllSkipLimitUser = async (req, res) => { //gộp của 2 hàm trên
    //B1: thu thập dữ liệu
    var skip = req.query.skip;
    console.log(skip);
    var limit = req.query.limit;
    console.log(limit);
    //B2: kiểm tra
    //B3: thực thi model
    try {
        const userList = await userModel.find().skip(skip).limit(limit);

        if (userList && userList.length > 0) {
            return res.status(200).json({
                status: "Get all user skip limit sucessfully",
                data: userList
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const getAllSortSkipLimitUser = async (req, res) => { //gộp của 3 hàm trên
    //B1: thu thập dữ liệu
    var skip = req.query.skip;
    console.log(skip);
    var limit = req.query.limit;
    console.log(limit);
    //B2: kiểm tra
    //B3: thực thi model
    try {
        const userList = await userModel.find().sort({ fullName: 1}).skip(skip).limit(limit);

        if (userList && userList.length > 0) {
            return res.status(200).json({
                status: "Get all user sort skip limit sucessfully",
                data: userList
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUser,
    deleteUser,
    getAllLimitUser,
    getAllSkipUser,
    getAllSortUser,
    getAllSkipLimitUser,
    getAllSortSkipLimitUser,
}