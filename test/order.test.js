const expect = require("chai").expect;
const request = require("supertest");
const orderModel = require("../app/models/order.model");
const userModel = require("../app/models/user.model");
const app = require("../index");
const mongoose = require('mongoose');

describe("/api/v1/orders", () => {
    var userId = "";

    before(async () => {
        await orderModel.deleteMany({});
        //get tổng số lượng user từ user model
        const userCount = await userModel.count();
        //lấy random 1 số từ số tổng user
        const random = await Math.floor(Math.random() * userCount);
        //lấy 1 user random từ userModel
        const userRandom = await userModel.findOne().skip(random).exec();
        userId = userRandom._id;
    });

    // after(async () => {
    //     mongoose.disconnect();
    // });

    it("should connect and disconnect to mongodb", async () => {
        // console.log(mongoose.connection.states);
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
            expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
            expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
            expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365");
    });

    describe("GET /", () => {
        it("should return all orders", async () => {
            const orders = [
                {
                    orderCode: "code 1",
                    pizzaSize: "M",
                    pizzaType: "Hawaii",
                    voucher: "64c7e4a4aa8bd908250d6c1c",
                    drink: "64c7e4a4aa8bd908250d6beb",
                    status: "open"
                },
                {
                    orderCode: "code 2",
                    pizzaSize: "M",
                    pizzaType: "Hawaii",
                    voucher: "64c7e4a4aa8bd908250d6c1c",
                    drink: "64c7e4a4aa8bd908250d6beb",
                    status: "open"
                },
            ];
            await orderModel.insertMany(orders);
            const res = await request(app).get("/api/v1/orders");
            expect(res.status).to.equal(200);
            expect(res.body.data.length).to.equal(2);
        });
        it("should return all orders of user", async () => {
            const res = await request(app).get("/api/v1/orders?userId=" + userId);
            expect(res.status).to.equal(200);
        });
    });

    describe("GET /:orderId", () => {
        it("should return a order if valid id is passed", async () => {
            const user = {
                _id: new mongoose.Types.ObjectId(),
                userId: userId,
                orderCode: "code 3",
                pizzaSize: "M",
                pizzaType: "Hawaii",
                voucher: "64c7e4a4aa8bd908250d6c1c",
                drink: "64c7e4a4aa8bd908250d6beb",
                status: "open"
            };
            await orderModel.create(user);
            const res = await request(app).get("/api/v1/orders/" + user._id);
            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("orderCode", user.orderCode);
            expect(res.body.data).to.have.property("status", user.status);
        });

        it("should return 400 error when invalid object id is passed", async () => {
            const res = await request(app).get("/api/v1/orders/abc");
            expect(res.status).to.equal(400);
        });

        it("should return 404 error when valid object id is passed but does not exist", async () => {
            const res = await request(app).get("/api/v1/orders/5f43ef20c1d4a133e4628181");
            expect(res.status).to.equal(404);
        });
    });

    describe("POST /", () => {
        it("should return order when the all request body is valid", async () => {
            const res = await request(app)
                .post("/api/v1/orders")
                .send({
                    userId: userId,
                    orderCode: "code 4",
                    pizzaSize: "M",
                    pizzaType: "Hawaii",
                    voucher: "64c7e4a4aa8bd908250d6c1c",
                    drink: "64c7e4a4aa8bd908250d6beb",
                    status: "open"
                });
            const data = res.body;
            expect(res.status).to.equal(201);
            expect(data.data).to.have.property("_id");
            expect(data.data).to.have.property("orderCode", "code 4");
            expect(data.data).to.have.property("pizzaSize", "M");
            expect(data.data).to.have.property("pizzaType", "Hawaii");

            const order = await orderModel.findOne({ orderCode: "code 4" });
            expect(order.pizzaSize).to.equal('M');
            expect(order.pizzaType).to.equal("Hawaii");
        });
    });

    describe("PUT /:orderId", () => {
        it("should update the existing order and return 200", async () => {
            const order = {
                _id: new mongoose.Types.ObjectId(),
                userId: userId,
                orderCode: "code 5",
                pizzaSize: "M",
                pizzaType: "Hawaii",
                voucher: "64c7e4a4aa8bd908250d6c1c",
                drink: "64c7e4a4aa8bd908250d6beb",
                status: "open"
            };
            await orderModel.create(order);

            const res = await request(app)
                .put("/api/v1/orders/" + order._id)
                .send({
                    orderCode: "code 5",
                    pizzaSize: "L",
                    pizzaType: "Bacon",
                    voucher: "64c7e4a4aa8bd908250d6c1c",
                    drink: "64c7e4a4aa8bd908250d6beb",
                    status: "done"
                });

            expect(res.status).to.equal(200);
            const dataAfterUpdate = await orderModel.findById(order._id).exec();
            expect(dataAfterUpdate).to.have.property("pizzaSize", "L");
            expect(dataAfterUpdate).to.have.property("pizzaType", "Bacon");
            expect(dataAfterUpdate).to.have.property("status", "done");
        });
    });

    let orderId = '';
    describe("DELETE /:orderId", () => {
        it("should delete requested id and return response 200", async () => {
            const order = {
                _id: new mongoose.Types.ObjectId(),
                userId: userId,
                orderCode: "code 6",
                pizzaSize: "M",
                pizzaType: "Hawaii",
                voucher: "64c7e4a4aa8bd908250d6c1c",
                drink: "64c7e4a4aa8bd908250d6beb",
                status: "open"
            };
            await orderModel.create(order);
            orderId = order._id;
            const res = await request(app).delete("/api/v1/orders/" + orderId + "?userId=" + userId);
            expect(res.status).to.be.equal(200);
        });

        it("should return 404 when deleted order is requested", async () => {
            let res = await request(app).get("/api/v1/orders/" + orderId + "?userId=" + userId);
            expect(res.status).to.be.equal(404);
        });
    });
});